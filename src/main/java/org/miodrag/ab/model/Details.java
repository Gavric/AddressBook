package org.miodrag.ab.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Details {
	
	public enum Type {
		PHONE_NUMBER, EMAIL, ADDRESS, NOTE
	}
	
	@Id
	@Column(name="DETAIL_ID", nullable=false, unique=true)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	
	@Column(name="DETAIL_TYPE")
	@Enumerated(EnumType.STRING)
	private Type type;
	
	@Column(name="DETAIL_VALUE")
	private String value;
	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DETAIL_ENTRY")
	private Entry entry;
	
	

}

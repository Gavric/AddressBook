package org.miodrag.ab;

import java.util.List;

import org.miodrag.ab.model.Entry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntryRepository extends JpaRepository<Entry,Long>{
	
	List<Entry> findByFirstName(String firstName);
	
}
